## 预期目标

- 高并发情形下，满足大量客户端的连接请求
- 高并发情形下，满足大量客户端的数据传输

## 解决思路

- ### 关键实现
  - 满功率运载多核**cpu**，每个cpu都要时刻处理客户端的连接请求
  - 满功率运载多核**cpu**，每个cpu都要时刻处理客户端的数据传输
- ### 约束条件
  - 通过多线程满功率运载多核**cpu**
  - 不能阻塞线程，影响**cpu**运行效率

## 期望结果

1. 创建管理**IO**对象
2. 创建侦听**socket**对象并将其绑定到管理**IO**对象中
3. 为侦听**socket**对象创建**n**个**accept**请求，**n**为**cpu x 2**
4. 创建**n**个工作线程，阻塞式获取管理**IO**对象的完成请求通知
   - 为侦听**socket**对象始终保持**n**个**accept**请求，**满足大量客户端的连接请求目标**
   - 将客户端**socket**绑定到管理**IO**对象中，然后为其投递**IO**请求
   - 在工作线程中处理关于客户端**socket**的完成请求通知，**注意不能阻塞工作线程**，**满足大量客户端的数据传输目标**

## 核心接口

1. **CreateIoCompletionPort**
   ```c_cpp
   HANDLE
   WINAPI
   CreateIoCompletionPort(
       _In_ HANDLE FileHandle,
       _In_opt_ HANDLE ExistingCompletionPort,
       _In_ ULONG_PTR CompletionKey,
       _In_ DWORD NumberOfConcurrentThreads
       );
   ```
   - 创建管理**IO**对象时前三个参数没有意义，第四个参数的真是含义是"真正并发同时执行的最大线程数"
   - 如何设置并发数就是个设计决策问题，决策的依据就是你的回调函数究竟要干些什么活，如果是时间较长的活计，就要考虑切换其它线程池来完成，如果是等待性质的活计，比如访问硬盘，等待某个事件等，就可以设置高一点的并发值，强制系统切换线程造成“伪并发”，如果是非常快速的活计，那么就直接设置**CPU**个数的并发数就行了，这时候防止线程频繁切换是首要任务。当然并发数最好是跟踪调试一下后再做决定，默认的推荐值就是**CPU**个数的2倍了
   - ```c_cpp
     HANDLE hIOCP = CreateIoCompletionPort(INVALID_HANDLE_VALUE,NULL,0,1);
     ```
   - 该函数同时可以将指定**socket**对象绑定到管理**IO**对象，**CompletionKey**参数会和该**socket**对象绑定，每次获取完成通知时可以获取该参数
2. **GetQueuedCompletionStatus**
   ```c_cpp
   BOOL
   WINAPI
   GetQueuedCompletionStatus(
       _In_ HANDLE CompletionPort,
       _Out_ LPDWORD lpNumberOfBytesTransferred,
       _Out_ PULONG_PTR lpCompletionKey,
       _Out_ LPOVERLAPPED* lpOverlapped,
       _In_ DWORD dwMilliseconds
       );
   ```
   - **GetQueuedCompletionStatus**会让工作线程挂起，有完成通知时唤醒当前工作线程，唤醒次序是后进先出，最后沉睡的先唤醒
   - **lpCompletionKey**参数绑定对应**socket**对象，**lpOverlapped**对象绑定对应**socket**对象上的指定的IO操作
3. **PostQueuedCompletionStatus**
   ```c_cpp
   BOOL
   WINAPI
   PostQueuedCompletionStatus(
       _In_ HANDLE CompletionPort,
       _In_ DWORD dwNumberOfBytesTransferred,
       _In_ ULONG_PTR dwCompletionKey,
       _In_opt_ LPOVERLAPPED lpOverlapped
       );
   ```
   - 参数与**GetQueuedCompletionStatus**类似，用来发送自定义的完成通知。通常在需要停止整个**IOCP**线程池工作时，就可以调用这个函数发送一个特殊的标志，比如设定**dwCompletionKey**为NULL，并且在自定义**lpOverlapped**指针结构之后带上一个表示关闭的标志等。这样在线程函数中就可以通过判定这些条件而明确的知道当前线程池需要关闭。
4. **投递请求API**
   ```c_cpp
   BOOL AcceptEx(  
     _In_  SOCKET       sListenSocket,  
     _In_  SOCKET       sAcceptSocket,  
     _In_  PVOID        lpOutputBuffer,  
     _In_  DWORD        dwReceiveDataLength,  
     _In_  DWORD        dwLocalAddressLength,  
     _In_  DWORD        dwRemoteAddressLength,  
     _Out_ LPDWORD      lpdwBytesReceived,  
     _In_  LPOVERLAPPED lpOverlapped  
   );
   ```
   - 用于投递**accept**请求，其中**sAcceptSocket**参数为预先准备好的客户端**socket**对象
   ```c_cpp
   int
   WSAAPI
   WSASend(
       _In_ SOCKET s,
       _In_reads_(dwBufferCount) LPWSABUF lpBuffers,
       _In_ DWORD dwBufferCount,
       _Out_opt_ LPDWORD lpNumberOfBytesSent,
       _In_ DWORD dwFlags,
       _Inout_opt_ LPWSAOVERLAPPED lpOverlapped,
       _In_opt_ LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine
       );
   ```
   - 用于投递**send**请求
   ```c_cpp
   int
   WSAAPI
   WSARecv(
       _In_ SOCKET s,
       _In_reads_(dwBufferCount) __out_data_source(NETWORK) LPWSABUF lpBuffers,
       _In_ DWORD dwBufferCount,
       _Out_opt_ LPDWORD lpNumberOfBytesRecvd,
       _Inout_ LPDWORD lpFlags,
       _Inout_opt_ LPWSAOVERLAPPED lpOverlapped,
       _In_opt_ LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine
       );
   ```
   - 用于投递**recv**请求
